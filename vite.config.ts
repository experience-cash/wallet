import { defineConfig } from 'vite'
import { nodePolyfills } from 'vite-plugin-node-polyfills'

export default defineConfig({
  base: '',
  build: {
    target: 'modules',
    outDir: './public',
  },
  optimizeDeps:
  {
    esbuildOptions: { target: 'esnext' },
  },
  esbuild: {
    supported: {
      'top-level-await': true
    },
  },
  plugins: [
    nodePolyfills(),
  ]
});
